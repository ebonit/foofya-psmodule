<div class="row">

	<div class="col-lg-12">
		<div class="panel">
			<legend>
			    foof<span style='color:orange;'>Y</span>a {l s='Payment Information'  mod='foofya'}
			</legend>
			
			<div id="info">
				<table style="border-spacing: 10px;">
				<tr>
					<td class="ffy">{l s='Reference:' mod='foofya'}</td>
					<td>{$reference}</td>
				</tr>
				<tr>
					<td class="ffy">{l s='CryptoCurrency:' mod='foofya'}</td>
					<td>{$cryptoCurrency}</td>
				</tr>
				<tr>
					<td class="ffy">{l s='Crypto Address:' mod='foofya'}</td>
					<td>{$address}</td>
				</tr>
				<tr>
					<td class="ffy">{l s='Crypto Amount:' mod='foofya'}</td>
					<td>{$invoiceAmount} {$cryptoCurrency}</td>
				</tr>
				<tr>
					<td class="ffy">{l s='Crypto Amount Paid:' mod='foofya'}</td>
					<td>{$invoicePaid} {$cryptoCurrency}</td>
				</tr>
				<tr>
					<td class="ffy">{l s='Crypto Amount Due:' mod='foofya'}</td>
					<td>{$amountDue}  {$cryptoCurrency}</td>
				</tr>
				<tr>
					<td class="ffy">{l s='Order Amount:' mod='foofya'}</td>
					<td>{$fiatAmount} {$fiatCurrency}</td>
				</tr>
				<tr>
					<td class="ffy">{l s='Order Amount Paid:' mod='foofya'}</td>
					<td>{$fiatAmountPaid} {$fiatCurrency}</td>
				</tr>
				<tr>
					<td class="ffy">{l s='Order Amount Due:' mod='foofya'}</td>
					<td>{$fiatAmountDue} {$fiatCurrency}</td>
				</tr>
				<tr>
					<td class="ffy">{l s='Status:' mod='foofya'}</td>
					<td>{$status}</td>
				</tr>
				<tr>
					<td class="ffy">{l s='Confirmed:' mod='foofya'}</td>
					<td>{$confirmed}</td>
				</tr>
				
				</table>
			</div>
		</div>
	</div>
</div>
