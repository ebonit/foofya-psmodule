<?php

class FoofyaPayment {
    
    public static $definition = [
        'table'=> 'foofya_payment',
        'primary' =>'id_foofya_payment',
        'multilang' => false,
        'fields' => [
            'id_foofya_payment' =>  ['type' => self::TYPE_INT, 'validate' => 'isUnsignedInt', 'required' => true],
            'id_order' =>           ['type' => self::TYPE_INT, 'validate' => 'isUnsignedInt', 'required' => true],
            'id_cart' =>            ['type' => self::TYPE_INT, 'validate' => 'isUnsignedInt', 'required' => true],
            'reference' =>          ['type' => self::TYPE_STRING, 'lang' => false, 'validate' => 'isGenericName', 'size' => 24],
            'cryptoCurrency' =>     ['type' => self::TYPE_STRING, 'lang' => false, 'validate' => 'isGenericName', 'size' => 10],
            'address' =>            ['type' => self::TYPE_STRING, 'lang' => false, 'validate' => 'isGenericName', 'size' => 255],
            'invoiceAmount' =>      ['type' => self::TYPE_FLOAT, 'vaidate' => 'isFloat'],
            'invoicePaid' =>        ['type' => self::TYPE_FLOAT, 'vaidate' => 'isFloat'],
            'amountDue' =>          ['type' => self::TYPE_FLOAT, 'vaidate' => 'isFloat'],
            'fiatAmount' =>         ['type' => self::TYPE_FLOAT, 'vaidate' => 'isPrice'],
            'fiatAmountPaid' =>     ['type' => self::TYPE_FLOAT, 'vaidate' => 'isPrice'],
            'fiatAmountDue' =>      ['type' => self::TYPE_FLOAT, 'vaidate' => 'isPrice'],
            'fiatCurrency' =>       ['type' => self::TYPE_STRING, 'lang' => false, 'validate' => 'isGenericName', 'size' => 3],
            'status' =>             ['type' => self::TYPE_TINYINT, 'size' => 2],
            'confirmed' =>          ['type' => self::TYPE_BOOL],
        ],
    ];
    
    public static function getIdByReference($reference)
    {
        $sql = new DbQuery();
        $sql->select('id_foofya_payment')
            ->from('foofya_payment')
            ->where('`reference` = '.$reference);        

        $result = Db::getInstance()->getValue($sql);

        return !empty($result) ? (int) $result : false;
    }
    
    public static function getByReference($reference)
    {
        $id_foofya_payment = (int) self::getIdByReference($reference);
        return ($id_foofya_payment > 0) ? new self($id_foofya_payment) : null;
    }
    
    public static function getIdByIdOrder($id_order)
    {
        $sql = new DbQuery();
        $sql->select('id_foofya_payment')
            ->from(self::$definition['table'])
            ->where('`id_order` = '.$id_order);        

        $result = Db::getInstance()->getValue($sql);

        return !empty($result) ? (int) $result : false;
    }
    
    public static function getByIdOrder($id_order){
        $id_foofya_payment = (int) self::getIdByIdOrder($id_order);
        return ($id_foofya_payment > 0) ? new self($id_foofya_payment) : null;
    }
}

