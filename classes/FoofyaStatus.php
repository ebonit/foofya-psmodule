<?php
/* 
 * @author  Eric Bontenbal
 * @copyright ©2020, Eric Bontenbal Web Development
 *  
 */

class FoofyaStatus {
    
    protected static $statusses = array(
        1 => 'Waiting for payment',
        2 => 'Late payment: Waiting for confirmation',
        3 => 'Paid',
        4 => 'Paid with less then expected',
        5 => 'Paid with more then expected',
        6 => 'Late payment: Paid',
        7 => 'Late payment: Paid with less then expected',
        8 => 'Late payment: Paid with more then expected'        
    );
    
    public static function getStatus($statusId){
        return self::$statusses[$statusId];
    }
}
    