<?php

/* 
 * @author  Eric Bontenbal
 * @copyright ©2016, Eric Bontenbal Web Development
 *  
 * 
 *  3Wa%0iEXXwh&
 */

class FoofyaApi {
    
    protected $environment;
    
    protected $apiKey;
    protected $secretKey;
    
    protected $url;
   
    
    public function __construct($apiKey, $secretKey){
        $this->apiKey = $apiKey;
        $this->secretKey = $secretKey; 
        
        // $this->url = "https://foofya.devphp7.nr1net.corp/api";
        $this->url = "https://foofya.com/api";    
        
        if($this->url == 'https://foofya.com/api'){
            $this->environment = 'production';
        }else{
            //in case we use a dev server
            $this->environment = 'development';
        }
    }
    
    public function invoiceCreate(Array $params = []){
        $postData = $this->createPost($params);
        $url = $this->url . "/paymentCreate";
        return $this->connect($url, $postData);
    }
    
    protected function createPost($params){
        if(null === $params){
            return $params;
        }
        
        $data = [
            "apikey"        => $this->apiKey,
            "fiatAmount"    => $params['fiatAmount'],
            "fiatCurrency"  => $params['fiatCurrency'],
            "order_id"      => $params['order_id'],
            "description"   => $params['description'],
            "callbackUrl"   => $params['callbackUrl'],
            "successUrl"    => $params['successUrl'],
            "cancelUrl"     => $params['cancelUrl'],
            "customParams"  => $params['customParams'],
        ];
        $data["secureKey"] = $this->createSecureKey($data);
        
        return json_encode($data);
    }
    
    protected function createSecureKey($requestArray){
        $string = json_encode($requestArray);
        return hash('sha256', $string.$this->secretKey);
    }
    
    protected function connect($url, $data){

        $ch = curl_init();        
        curl_setopt($ch, CURLOPT_HTTPHEADER, [                                                                          
            'Content-Type: application/json',                                                                                
            'Content-Length: ' . strlen($data)
        ]);
        
        curl_setopt($ch, CURLOPT_URL, $url);                                                                      
        curl_setopt($ch, CURLOPT_POST, true);                                                                     
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);                                                                  
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);   
        if($this->environment == 'production'){
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER,1);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST,2);
        }else{
            //development only
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER,0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST,0);
        }                                                   
                                                                                                                     
        $result = curl_exec($ch);
        $errors = curl_error($ch);
        $response = curl_getinfo($ch);
        curl_close($ch);
        if($errors){
//nakijken!!! beter afhandelen...

            $result['errors'] = var_export($errors,1);
            $result = json_encode($result);
        }
        
        if($this->isJson($result)){
            return json_decode($result);
        }else{
            die($result . '<div style="width:600px;text-align:center;margin:auto;">credentials problem at foofya payments.<br />If this message persists, please contact support.</div>');
            return false;
        }  
    }

    protected function isJson($object){
        $test = json_decode($object);
        return (json_last_error() == JSON_ERROR_NONE);
    }
}
