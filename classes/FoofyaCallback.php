<?php
/* 
 * @author  Eric Bontenbal
 * @copyright ©2020, Eric Bontenbal Web Development
 *  
 */
 
require_once(_PS_MODULE_DIR_.'foofya/foofya.php');

class FoofyaCallback {
    
    protected $secret;
    
    public function __construct($secret){
        $this->secret = $secret;
    }
    
    public function validate($jsonObj){
        $array = json_decode($jsonObj,1);
        $secureKey = $array['secureKey'];
        unset($array['secureKey']);
        $string = json_encode($array);
Foofya::log(__FILE__.__LINE__.PHP_EOL.($string));
        // return ( $secureKey === hash('sha256', $string.$this->secret) );
        return hash_equals( $secureKey, hash('sha256', $string.$this->secret) );
    }
}
