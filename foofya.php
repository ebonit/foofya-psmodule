<?php
/* 
 * @author  Eric Bontenbal
 * @copyright ©2020, Eric Bontenbal Web Development
 *  
 */
 
if (!defined('_PS_VERSION_'))
exit;

use PrestaShop\PrestaShop\Core\Payment\PaymentOption;
require_once(__DIR__ . '/classes/FoofyaApi.php');
include_once(__DIR__ . '/classes/FoofyaPayment.php');
include_once(__DIR__ . '/classes/FoofyaStatus.php');

class Foofya extends PaymentModule
{
    private $_html = '';
    private $_postErrors = array();
    public $method;

    function __construct()
    {
        $this->name = 'foofya';
        $this->tab = 'payments_gateways';
        $this->version = '1.0.0';
        $this->author = 'foofya.com';
        $this->method = 'foofYa';

        $this->page = basename(__FILE__, '.php');
        $this->displayName = $this->l('Foofya crypto payments');
        $this->description = $this->l('Accept payments in bitcoin and other crypto currencies through foofYa.com.');
        
        $this->ps_versions_compliancy = [
            'min' => '1.7.6',
            'max' => _PS_VERSION_
        ];
        
        parent::__construct();
        
        if (Configuration::get('FOOFYA_APIKEY') == '')
        {
            $this->warning .= $this->l('Invoice API key is empty.');
        }

        if (Configuration::get('FOOFYA_APISECRET') == '')
        {
            $this->warning .= $this->l('API secret key is empty.');
        }		
    }

    public function install()
    {
        

        if (!parent::install() || 
            !$this->registerHook('paymentOptions') || 
            !$this->registerHook('displayPaymentReturn') || 
            !$this->registerHook('displayHeader') || 
            !$this->registerHook('displayBackOfficeHeader') || 
            !$this->registerHook('displayAdminOrderLeft') )
        {
            return false;
        }

        $db = Db::getInstance();
        $query = "CREATE TABLE IF NOT EXISTS `" . _DB_PREFIX_ . "foofya_payment` (
          `id_foofya_payment` int(11) NOT NULL AUTO_INCREMENT,
          `id_order` int(11) NOT NULL DEFAULT '0',
          `id_cart` int(11) NOT NULL,
          `reference` varchar(128) NOT NULL,
          `cryptoCurrency` varchar(6) DEFAULT NULL,
          `address` varchar(255) NOT NULL,
          `invoiceAmount` decimal(12,8) NOT NULL DEFAULT '0.00000000',
          `invoicePaid` decimal(12,8) NOT NULL DEFAULT '0.00000000',
          `amountDue` decimal(12,8) NOT NULL DEFAULT '0.00000000',
          `fiatAmount` decimal(6,2) NOT NULL DEFAULT '0.00',
          `fiatAmountPaid` decimal(6,2) NOT NULL DEFAULT '0.00',
          `fiatAmountDue` decimal(6,2) NOT NULL DEFAULT '0.00',
          `fiatCurrency` varchar(3) NOT NULL,
          `status` tinyint(2) DEFAULT NULL,
          `confirmed` tinyint(1) NOT NULL DEFAULT '0',
          PRIMARY KEY (`id_foofya_payment`),
          UNIQUE KEY `invoice` (`reference`)
        ) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8";

        $db->Execute($query);

        Configuration::updateValue('FOOFYA_APIKEY', '');
        Configuration::updateValue('FOOFYA_APISECRET', '');
        Configuration::updateValue('FOOFYA_TRESHOLD', 95);
        
        $this->_createStates();

        return true;
    }

    public function getContent()
    {
        $this->_html .= '<h2>'.$this->l('foofYa').'</h2>';	

        $this->_postProcess();
        $this->_setFoofyaSubscription();
        $this->_setConfigurationForm();

        return $this->_html;
    }
    
    public function hookDisplayHeader()
    {
        $this->context->controller->addCSS($this->_path.'css/foofya.css', 'all');
    }

    public function hookDisplayBackOfficeHeader()
    {
        $this->context->controller->addCSS($this->_path.'css/foofya.css', 'all');
    }
    
    public function hookPaymentOptions($params)
    {
        if (!$this->active) {
            return;
        }
        
        $newOption = new PaymentOption();

        $newOption->setModuleName($this->name)
                ->setCallToActionText('Pay with Bitcoin')
                ->setAction($this->context->link->getModuleLink('foofya', 'payment', []));
             
        $payment_options = [
            $newOption,
        ];

        return $payment_options;
    }

    private function _setFoofyaSubscription()
    {
        $this->_html .= '
        <b>'.$this->l('This module allows you to accept payments in bitcoin through foofya.com.').'</b><br /><br />
        '.$this->l('If the client chooses this payment mode, your foofYa account will be automatically credited.').'<br />
        '.$this->l('You need to configure your foofYa account before using this module.').'
        <div style="clear:both;">&nbsp;</div>';
    }

    private function _setConfigurationForm()
    {
        $this->_html .= '
        <form method="post" action="'.htmlentities($_SERVER['REQUEST_URI']).'">	
            <script type="text/javascript">
                var pos_select = '.(($tab = (int)Tools::getValue('tabs')) ? $tab : '0').';
            </script>
            <script type="text/javascript" src="'._PS_BASE_URL_._PS_JS_DIR_.'tabpane.js"></script>
            <link type="text/css" rel="stylesheet" href="'._PS_BASE_URL_._PS_CSS_DIR_.'tabpane.css" />
            <input type="hidden" name="tabs" id="tabs" value="0" />
            <div class="tab-pane" id="tab-pane-1" style="width:100%;">
                <div class="tab-page" id="step1">
                    <h4 class="tab">'.$this->l('Settings').'</h2>
                    '.$this->_getSettingsTabHtml().'
                </div>
            </div>
            <div class="clear"></div>
            <script type="text/javascript">
                function loadTab(id){}
                setupAllTabs();
            </script>
        </form>';
    }

    private function _getSettingsTabHtml()
    {
        $html = '
        <label>'.$this->l('Invoice API key').':</label>
        <div class="margin-form">
            <input type="text" name="apikey_foofya" value="'.htmlentities(Tools::getValue('apikey_foofya', Configuration::get('FOOFYA_APIKEY')), ENT_COMPAT, 'UTF-8').'" size="40" />
        </div>
        <label>'.$this->l('API secret').':</label>
        <div class="margin-form">
            <input type="text" name="secret_foofya" value="'.htmlentities(Tools::getValue('secret_foofya', Configuration::get('FOOFYA_APISECRET')), ENT_COMPAT, 'UTF-8').'" size="40" />
        </div> ' . 
        '<label>'.$this->l('Accept Treshold').':</label>
        <p>(This is the percentage of the amount that is still acceptable to be set to paid automatically. The default is 95%. Setting this to a value higher then 100 or to zero will set the treshold to 100) </p>
       
        <div class="margin-form">
            <input type="text" name="treshold_foofya" value="'.htmlentities(Tools::getValue('treshold_foofya', Configuration::get('FOOFYA_TRESHOLD')), ENT_COMPAT, 'UTF-8').'" size="40" />
        </div>
        <p><input class="button" type="submit" name="submitfoofya" value="'.$this->l('Save settings').'" /></p>';
        return $html;
    }

    private function _postProcess()
    {

        if (Tools::isSubmit('submitfoofya'))
        {
            $template_available = array('A', 'B', 'C');

            $this->_errors = array();

            if (Tools::getValue('apikey_foofya') == NULL)
            {
                $this->_errors[] = $this->l('Missing foofya invoice API key');
            }

            if (Tools::getValue('secret_foofya') == NULL)
            {
                $this->_errors[] = $this->l('Missing foofya Secret');
            }

            if (count($this->_errors) > 0)
            {
                $error_msg = '';
                foreach ($this->_errors AS $error)
                    $error_msg .= $error.'<br />';
                $this->_html = $this->displayError($error_msg);
            }
            else
            {
                
                Configuration::updateValue('FOOFYA_APIKEY', trim(Tools::getValue('apikey_foofya')));
                Configuration::updateValue('FOOFYA_APISECRET', trim(Tools::getValue('secret_foofya')));
                if(trim(Tools::getValue('treshold_foofya')) == '' || trim(Tools::getValue('treshold_foofya')) == 0 || trim(Tools::getValue('treshold_foofya')) > 100){
                    $treshold = 100;
                }else{
                    $treshold = trim(Tools::getValue('treshold_foofya'));
                }
                Configuration::updateValue('FOOFYA_TRESHOLD', $treshold);

                $this->_html = $this->displayConfirmation($this->l('Settings updated'));
            }
        }
    }

    public function execPayment($cart) {

        if (!$this->active)
            return;
        
        if($cart !== $this->context->cart){
            return;
        }


        $amount = number_format($cart->getOrderTotal(true), 2, '.', '');
        $currency = new Currency((int)($cart->id_currency));
        $currency = $currency->iso_code;	
        
        $orderState = (int)Configuration::get('FOOFYA_OS_PENDING');
        
        //first we need to validate the order. 
        //if we send the cart instead of the order, the cart can be changed just befor payment 
        //resulting in a wrong payment amount.
        $this->validateOrder(
            $cart->id,//this is the cart id
            $orderState,
            $amount,
            $this->displayName,
            null,
            array(),
            null,
            false,
            $cart->secure_key
        );

        $order = new Order($this->currentOrder);  	
        $custom = array(
            'cart_id' => $cart->id,
            'key' => $this->context->customer->secure_key
        );
        
        
        $api = new FoofyaAPI(Configuration::get('FOOFYA_APIKEY'), Configuration::get('FOOFYA_APISECRET'));
        $callback_url =$this->context->link->getModuleLink('foofya', 'callback', []);
        
        $return_url = $this->context->link->getModuleLink('foofya', 'return', ['oid' => $order->id, 'ref' => hash('sha256', $order->id . Configuration::get('FOOFYA_APISECRET'))]);
        $cancel_url = $this->context->link->getModuleLink('foofya', 'cancel', ['oid' => $order->id, 'ref' => hash('sha256', $order->id . Configuration::get('FOOFYA_APISECRET'))] );

        $result = $api->invoiceCreate([
            "fiatAmount"    => $amount,
            "fiatCurrency"  => $currency,
            "order_id"      => $order->id,
            "description"   => 'Order '.$cart->id,
            "callbackUrl"   => $callback_url,
            "successUrl"    => $return_url,
            "cancelUrl"     => $cancel_url,
            "customParams"  => $custom
        ]);
        

        if(isset($result->errors)){
            print 'An error occurred.';
            print_r($result->errors);
        }
        else {
            //omdet we hier geen template returnen geeft dit een warning. dus als de debugger aanstaat gaat die over zijn nek.
            header('Location: ' . $result->redirectUrl);
        }
        exit();
        
    }

    function writeDetails($id_order, $order_status, $cbObj) {
        $db = Db::getInstance();
        try{
            
            $foofyaPayment = FoofyaPayment::getByReference($cbObj->invoice->reference);
            if(!$foofyaPayment){
                $foofyaPayment = new FoofyaPayment();
            }
            $foofyaPayment->id_order = $id_order;
            $foofyaPayment->id_cart = intval($cbObj->order->order_id);
            $foofyaPayment->reference = $cbObj->invoice->reference;
            $foofyaPayment->cryptoCurrency = $cbObj->invoice->cryptoCurrency;
            $foofyaPayment->address = $cbObj->invoice->invoiceAddress;
            $foofyaPayment->invoiceAmount = $cbObj->invoice->invoiceAmount;
            $foofyaPayment->invoicePaid = $cbObj->invoice->invoicePaid;
            $foofyaPayment->amountDue = $cbObj->invoice->amountDue;
            $foofyaPayment->fiatAmount = $cbObj->order->fiatAmount;
            $foofyaPayment->fiatAmountPaid = $cbObj->invoice->fiatAmountPaid;
            $foofyaPayment->fiatAmountDue = $cbObj->invoice->fiatAmountDue;
            $foofyaPayment->fiatCurrency = isset($cbObj->invoice->fiatCurrency) ? $cbObj->invoice->fiatCurrency : $cbObj->order->fiatCurrency;
            $foofyaPayment->status = isset($cbObj->invoice->status) ? $cbObj->invoice->status : 0;
            $foofyaPayment->confirmed = isset($cbObj->invoice->confirmed) ? $cbObj->invoice->confirmed : 0;
self::log(var_export($foofyaPayment, true));
            $foofyaPayment->save();
            
        }catch(Exception $e){
            self::log(__LINE__ . $e->getMessage());
        }

        try{
            if($id_order > 0){
                $result = $db->update('order_history',
                    'id_order_state = ' . $order_status,
                    'id_order = ' . intval($id_order)
                );
            }
            
        }catch(Exception $e){
            self::log(__LINE__ . $e->getMessage());
        }
    }

    
    
    function hookDisplayAdminOrderLeft($params){
        $id_order = $params['id_order'];
        try{
            $foofyaPayment = FoofyaPayment::getByIdOrder($id_order);
        }catch(Exception $e){
            die($e->getMessage);
        }
        if($foofyaPayment){
            $this->context->smarty->assign(array(
                'reference' => $foofyaPayment->reference,
                'address' => $foofyaPayment->address,
                'cryptoCurrency' => $foofyaPayment->cryptoCurrency,
                'invoiceAmount' => $foofyaPayment->invoiceAmount,
                'invoicePaid' => $foofyaPayment->invoicePaid,
                'amountDue' => $foofyaPayment->amountDue,
                'fiatAmount' => $foofyaPayment->fiatAmount,
                'fiatAmountPaid' => $foofyaPayment->fiatAmountPaid,
                'fiatAmountDue' => $foofyaPayment->fiatAmountDue,
                'fiatCurrency' => $foofyaPayment->fiatCurrency,
                'status' => FoofyaStatus::getStatus($foofyaPayment->status),
                'confirmed' => $foofyaPayment->confirmed,
                'id_order' => $id_order,
                'this_page' => $_SERVER['REQUEST_URI'],
                'this_path' => $this->_path,
                'this_path_ssl' => Configuration::get('PS_FO_PROTOCOL').$_SERVER['HTTP_HOST'].__PS_BASE_URI__."modules/{$this->name}/"
            ));
            return $this->display(__FILE__, 'invoice_block.tpl');
        }
        return;
    }

    function hookDisplayPaymentReturn($params) {
        try{
        $cart = $params['cart'];
        if($cart->orderExists()){
           $order = new Order((int)Order::getOrderByCartId($cart->id));
            $order_id = $order->id;
        }else{
            $order_id = 0;
        }
        
        }catch(Exception $e){
            die($e->getMessage());
        }
        
        $this->context->smarty->assign(array(
        'status' => 'ok',
        'this_path' => $this->_path,
        'this_path_ssl' => Configuration::get('PS_FO_PROTOCOL').$_SERVER['HTTP_HOST'].__PS_BASE_URI__."modules/{$this->name}/"));

        return $this->fetch('module:foofya/views/templates/hook/payment_return.tpl');
    }

    private function _createStates() {
        if (!Configuration::get('FOOFYA_OS_PENDING'))
        {
            $order_state = new OrderState();
            $order_state->name = array();
            foreach (Language::getLanguages() as $language)
                    $order_state->name[$language['id_lang']] = 'Awaiting Crypto payment';
            $order_state->send_email = false;
            $order_state->color = '#9d0096';
            $order_state->hidden = false;
            $order_state->delivery = false;
            $order_state->logable = false;
            $order_state->invoice = false;
            $order_state->add();
            Configuration::updateValue('FOOFYA_OS_PENDING', (int)$order_state->id);
        }
        
        if (!Configuration::get('FOOFYA_OS_CONFIRMATION'))
        {
            $order_state = new OrderState();
            $order_state->name = array();
            foreach (Language::getLanguages() as $language)
                    $order_state->name[$language['id_lang']] = 'Awaiting Crypto Confirmation';
            $order_state->send_email = false;
            $order_state->color = '#9d0096';
            $order_state->hidden = false;
            $order_state->delivery = false;
            $order_state->logable = false;
            $order_state->invoice = false;
            $order_state->add();
            Configuration::updateValue('FOOFYA_OS_CONFIRMATION', (int)$order_state->id);
        }

        if (!Configuration::get('FOOFYA_OS_PARTIAL'))
        {
            $order_state = new OrderState();
            $order_state->name = array();
            foreach (Language::getLanguages() as $language)
                    $order_state->name[$language['id_lang']] = 'Partial Crypto Payment';
            $order_state->send_email = false;
            $order_state->color = '#8F0621';
            $order_state->hidden = false;
            $order_state->delivery = false;
            $order_state->logable = false;
            $order_state->invoice = false;
            $order_state->add();
            Configuration::updateValue('FOOFYA_OS_PARTIAL', (int)$order_state->id);
        }

        if (!Configuration::get('FOOFYA_OS_EXPIRED'))
        {
            $order_state = new OrderState();
            $order_state->name = array();
            foreach (Language::getLanguages() as $language)
                    $order_state->name[$language['id_lang']] = 'Crypto Payment Expired';
            $order_state->send_email = false;
            $order_state->color = '#8F0621';
            $order_state->hidden = false;
            $order_state->delivery = false;
            $order_state->logable = false;
            $order_state->invoice = false;
            $order_state->add();
            Configuration::updateValue('FOOFYA_OS_EXPIRED', (int)$order_state->id);
        }
    }

    public static function log($message){
        $handle = @fopen(__DIR__.'/log/debug.log', 'a');
        $message = date("Y-m-d H:i:s") . " - {$message}\n";
        @fwrite($handle, $message);
        @fclose($handle);
    }
    
    //TODO: afmaken ivm overzicht van alle betalingen
    public function getList(){
        return FoofyaPayments::getList();
    }
    
    
    public function checkHash($reference, $hash){
        return hash_equals( $hash, hash('sha256', $reference.Configuration::get('FOOFYA_APISECRET')) );
    }
}
