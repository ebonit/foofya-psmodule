<?php
/* 
 * @author  Eric Bontenbal
 * @copyright ©2020, Eric Bontenbal Web Development
 *  
 */


class FoofyaReturnModuleFrontController extends ModuleFrontController{
    
    public $ssl = true;

    public function initContent()
    {
// die(var_dump($this->context->link));          
        
        $foofya = $this->module;
        //check the hash als extra security
        if(false === $foofya->checkhash(Tools::getValue('oid'), Tools::getValue('ref'))){                
            Tools::redirect('index.php');
        }        
        
        parent::initContent();
                
        //$order = new Order((int)Tools::getValue('oid')); 
                       
        // if(!$order)
        // {
            // $foofya->validateOrder(
                // $cart->id,
                // (int)Configuration::get('FOOFYA_OS_CONFIRMATION'),
                // $cart->getOrderTotal(),
                // $foofya->displayName,
                // null,
                // array(),
                // null,
                // false,
                // $cart->secure_key
            // );
        // }     
        Tools::redirect($this->context->link->getPageLink('history'));
    }
}