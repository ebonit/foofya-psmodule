<?php
/* 
 * @author  Eric Bontenbal
 * @copyright ©2020, Eric Bontenbal Web Development
 *  
 */

class FoofyaPaymentModuleFrontController extends ModuleFrontController{
    
    public $ssl = true;

    public function initContent()
    {
        parent::initContent();
        
        $cart = $this->context->cart;
// die(var_dump($this->module));
        $foofya = $this->module;
        $foofya::log('cart = '.$cart->id);        
        $foofya->execPayment($cart);
    }
}
