<?php
/* 
 * @author  Eric Bontenbal
 * @copyright ©2020, Eric Bontenbal Web Development
 *  
 */

include(_PS_MODULE_DIR_.'foofya/classes/FoofyaCallback.php');

class FoofyaCallbackModuleFrontController extends ModuleFrontController{
    
    public $ssl = true;

    public function initContent()
    {
        $foofya = $this->module;

        $body = file_get_contents('php://input');
        $obj = json_decode($body);
$foofya::log(var_export($obj,1));        
        switch (json_last_error()) {
        case JSON_ERROR_NONE:
            $foofya::log(__FILE__.__LINE__.PHP_EOL.' - No errors');
            break;
        case JSON_ERROR_DEPTH:
            $foofya::log(__FILE__.__LINE__.PHP_EOL.' - Maximum stack depth exceeded');
            break;
        case JSON_ERROR_STATE_MISMATCH:
            $foofya::log(__FILE__.__LINE__.PHP_EOL.' - Underflow or the modes mismatch');
            break;
        case JSON_ERROR_CTRL_CHAR:
            $foofya::log(__FILE__.__LINE__.PHP_EOL.' - Unexpected control character found');
            break;
        case JSON_ERROR_SYNTAX:
            $foofya::log(__FILE__.__LINE__.PHP_EOL.' - Syntax error, malformed JSON');
            break;
        case JSON_ERROR_UTF8:
            $foofya::log(__FILE__.__LINE__.PHP_EOL.' - Malformed UTF-8 characters, possibly incorrectly encoded');
            break;
        default:
            $foofya::log(__FILE__.__LINE__.PHP_EOL.' - Unknown error');
            break;
    }

        $callback = new FoofyaCallback(Configuration::get('FOOFYA_APISECRET'));
    
        if (!$callback->validate($body)) {
            $foofya::log('validateCallback() failed');
            return;
        }else{
            $foofya::log('validateCallback() success');
        }
        
        $order = new Order((int)$obj->order->order_id);
        
        switch ($obj->invoice->status) {
            case 1:
            case 2: {
                $new_status = (int)Configuration::get('FOOFYA_OS_PENDING');
                break;
            }
            case 3: { //op tijd betaald
                if($obj->invoice->confirmed === true){
                    $new_status = (int)Configuration::get('PS_OS_PAYMENT');
                }else{
                    $new_status = (int)Configuration::get('FOOFYA_OS_CONFIRMATION');
                }
                break;
            }//op tijd betaald
            case 5: //op tijd betaald en teveel
            case 8: { //te laat betaald en teveel
                if($obj->invoice->confirmed === true){
                    $new_status = (int)Configuration::get('PS_OS_PAYMENT');
                    //TODO: hier moet er een mailtje uit dat er teveel is betaald
                    // Mail::send();
                }else{
                    $new_status = (int)Configuration::get('FOOFYA_OS_CONFIRMATION');
                }
                break;
            }
            case 4: //op tijd betaald maar te weinig
            case 7: { //te laat betaald en te weinig
                if( (($obj->invoice->fiatAmountPaid / $obj->order->fiatAmount) * 100) < Configuration::get('FOOFYA_TRESHOLD') ){
                    // hier is het minder dan de treshold
                    $new_status = (int)Configuration::get('FOOFYA_OS_PARTIAL');
                    if($obj->invoice->confirmed === true){ 
                        //hier moet er een mailtje uit dat er te weinig is betaald en dat de order niet op betaald is gezet
                    }
                }else{
                    //hier zit het binnen de treshold dus mag op paid
                    if($obj->invoice->confirmed === true){
                        $new_status = (int)Configuration::get('PS_OS_PAYMENT');
                        //TODO: hier een mailtje dat er iets te weinig is betaald maar dat de order op paid is gezet
                    }else{
                        $new_status = (int)Configuration::get('FOOFYA_OS_CONFIRMATION');
                    }
                }
                break;
            }
        }
        
        
        if($new_status == (int)Configuration::get('FOOFYA_OS_PENDING')){
$foofya::log(__FILE__.__LINE__.PHP_EOL.'$new_status = FOOFYA_OS_PENDING: '.$new_status);            
            $foofya->writeDetails($order->id, $new_status, $obj);
            
        }else{        
$foofya::log('orderExists()');
$foofya::log('orderId = '.(int)$order->id);
$foofya::log('new status = '.(int)$new_status);
            $history = new OrderHistory();
            $history->id_order = (int)$order->id;
            $history->id_order_state = (int)$new_status;
            //$history->save();
            $history->changeIdOrderState((int)$new_status, $order->id, true);
            $history->save();
            
            $foofya->writeDetails((int)$order->id, $new_status, $obj);
           
        }
    }
}



