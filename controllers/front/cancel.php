<?php
/* 
 * @author  Eric Bontenbal
 * @copyright ©2020, Eric Bontenbal Web Development
 *  
 */

class FoofyaCancelModuleFrontController extends ModuleFrontController{
    
    public $ssl = true;

    public function initContent()
    {
        
        $foofya = $this->module;
        //check the hash als extra security
        if(false === $foofya->checkhash(Tools::getValue('oid'), Tools::getValue('ref'))){                
            Tools::redirect('index.php');
        } 

        $foofya::log('cancel order '.Tools::getValue('oid'));

        Tools::redirect($this->context->link->getPageLink('history'));                    
    }
}